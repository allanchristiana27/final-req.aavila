import 'package:flutter/material.dart';
import 'package:flutter/gestures.dart';
import 'dart:ui';
import 'package:google_fonts/google_fonts.dart';
import 'package:myapp/page-1/history.dart';
import 'package:myapp/page-1/home.dart';
import 'package:myapp/page-1/splash-screen.dart';
import 'package:myapp/utils.dart';

class User extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    double baseWidth = 430;
    double fem = MediaQuery.of(context).size.width / baseWidth;
    double ffem = fem * 0.97;
    return Material(
        child: Container(
            width: double.infinity,
            child: Container(
                // historyQFY (25:143)
                padding:
                    EdgeInsets.fromLTRB(40 * fem, 80 * fem, 40 * fem, 10 * fem),
                width: double.infinity,
                decoration: BoxDecoration(
                  color: Color(0xffeddfdf),
                ),
                child: Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Container(
                        child: SizedBox(
                          height: 200,
                          width: 300,
                          child:
                              Image.asset('assets/page-1/images/userphoto.png'),
                        ),
                      ),
                      SizedBox(
                        height: 10,
                      ),
                      Container(
                        child: Text(
                          'Allan Christian Avila',
                          style: TextStyle(
                            color: Colors.black,
                            fontSize: 20,
                          ),
                        ),
                      ),
                      SizedBox(
                        height: 20,
                      ),
                      Container(
                        child: Align(
                            alignment: Alignment.centerLeft,
                            child: Container(
                              child: Text(
                                'Email:',
                                style: TextStyle(
                                    fontWeight: FontWeight.bold,
                                    fontSize: 18,
                                    color: Colors.black),
                              ),
                            )),
                      ),
                      Container(
                        child: Align(
                            alignment: Alignment.centerLeft,
                            child: Container(
                              child: Text(
                                'allanchristian27@gmail.com',
                                style: TextStyle(
                                    fontSize: 18, color: Colors.black),
                              ),
                            )),
                      ),
                      SizedBox(
                        height: 10,
                      ),
                      Container(
                        child: Align(
                            alignment: Alignment.centerLeft,
                            child: Container(
                              child: Text(
                                'Number',
                                style: TextStyle(
                                    fontWeight: FontWeight.bold,
                                    fontSize: 18,
                                    color: Colors.black),
                              ),
                            )),
                      ),
                      Container(
                        child: Align(
                            alignment: Alignment.centerLeft,
                            child: Container(
                              child: Text(
                                '09776395104',
                                style: TextStyle(
                                    fontSize: 18, color: Colors.black),
                              ),
                            )),
                      ),
                      SizedBox(
                        height: 10,
                      ),
                      Container(
                        child: Align(
                            alignment: Alignment.centerLeft,
                            child: Container(
                              child: Text(
                                'Address',
                                style: TextStyle(
                                    fontWeight: FontWeight.bold,
                                    fontSize: 18,
                                    color: Colors.black),
                              ),
                            )),
                      ),
                      Container(
                        child: Align(
                            alignment: Alignment.centerLeft,
                            child: Container(
                              child: Text(
                                'Poblacion Balungan Pangasinan',
                                style: TextStyle(
                                    fontSize: 18, color: Colors.black),
                              ),
                            )),
                      ),
                      SizedBox(
                        height: 50,
                      ),
                      Container(
                        child: Row(
                          children: [
                            SizedBox(
                              height: 40,
                              width: 100,
                              child: TextButton(
                                  onPressed: () {
                                    Navigator.push(context, MaterialPageRoute(
                                        builder: (BuildContext context) {
                                      return History();
                                    }));
                                  },
                                  style: TextButton.styleFrom(
                                      shape: RoundedRectangleBorder(
                                          borderRadius: BorderRadius.zero,
                                          side: BorderSide(
                                            color: Colors.black,
                                          ))),
                                  child: Text(
                                    'History',
                                    style: TextStyle(
                                      color: Colors.black,
                                    ),
                                  )),
                            ),
                            Spacer(),
                            SizedBox(
                              height: 40,
                              width: 100,
                              child: TextButton(
                                  onPressed: () {
                                    Navigator.push(context, MaterialPageRoute(
                                        builder: (BuildContext context) {
                                      return Splash();
                                    }));
                                  },
                                  style: TextButton.styleFrom(
                                      shape: RoundedRectangleBorder(
                                          borderRadius: BorderRadius.zero,
                                          side: BorderSide(
                                            color: Colors.black,
                                          ))),
                                  child: Text('Logout',
                                      style: TextStyle(
                                        color: Colors.black,
                                      ))),
                            )
                          ],
                        ),
                      )
                    ]))));
  }
}
