import 'package:flutter/material.dart';
import 'package:flutter/gestures.dart';
import 'dart:ui';
import 'package:google_fonts/google_fonts.dart';
import 'package:myapp/page-1/purchased.dart';
import 'package:myapp/utils.dart';

class Hawaiian extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    double baseWidth = 430;
    double fem = MediaQuery.of(context).size.width / baseWidth;
    double ffem = fem * 0.97;
    return Container(
      width: double.infinity,
      child: Material(
        child: Container(
          // purchaseukA (25:86)
          padding: EdgeInsets.fromLTRB(33 * fem, 90 * fem, 33 * fem, 0 * fem),
          width: double.infinity,
          decoration: BoxDecoration(
            color: Color(0xffeddfdf),
          ),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Container(
                // rectangle61HQ (25:105)
                margin:
                    EdgeInsets.fromLTRB(1 * fem, 0 * fem, 0 * fem, 50 * fem),
                child: TextButton(
                  onPressed: () {},
                  style: TextButton.styleFrom(
                    padding: EdgeInsets.zero,
                  ),
                  child: Container(
                    width: 135 * fem,
                    height: 147 * fem,
                    child: ClipRRect(
                      borderRadius: BorderRadius.circular(30 * fem),
                      child: Image.asset(
                        'assets/page-1/images/rectangle-6.png',
                        fit: BoxFit.cover,
                      ),
                    ),
                  ),
                ),
              ),
              Container(
                // purchasegPY (25:108)
                margin:
                    EdgeInsets.fromLTRB(0 * fem, 0 * fem, 277 * fem, 17 * fem),
                child: Text(
                  'Purchase :',
                  style: SafeGoogleFont(
                    'JejuMyeongjo',
                    fontSize: 20 * ffem,
                    fontWeight: FontWeight.w400,
                    height: 1.2575 * ffem / fem,
                    color: Color(0xff000000),
                  ),
                ),
              ),
              Container(
                // ellipse10nSa (25:107)
                margin: EdgeInsets.fromLTRB(
                    115 * fem, 0 * fem, 114 * fem, 26 * fem),
                width: double.infinity,
                height: 135 * fem,
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(67.5 * fem),
                  border: Border.all(color: Color(0xff000000)),
                  image: DecorationImage(
                    fit: BoxFit.cover,
                    image: AssetImage(
                      'assets/page-1/images/hawaiian.jpg',
                    ),
                  ),
                  boxShadow: [
                    BoxShadow(
                      color: Color(0x3f000000),
                      offset: Offset(0 * fem, 4 * fem),
                      blurRadius: 2 * fem,
                    ),
                    BoxShadow(
                      color: Color(0x3f000000),
                      offset: Offset(0 * fem, 4 * fem),
                      blurRadius: 2 * fem,
                    ),
                    BoxShadow(
                      color: Color(0x3f000000),
                      offset: Offset(0 * fem, 4 * fem),
                      blurRadius: 2 * fem,
                    ),
                    BoxShadow(
                      color: Color(0x3f000000),
                      offset: Offset(0 * fem, 4 * fem),
                      blurRadius: 2 * fem,
                    ),
                    BoxShadow(
                      color: Color(0x3f000000),
                      offset: Offset(0 * fem, 4 * fem),
                      blurRadius: 2 * fem,
                    ),
                    BoxShadow(
                      color: Color(0x3f000000),
                      offset: Offset(0 * fem, 4 * fem),
                      blurRadius: 2 * fem,
                    ),
                    BoxShadow(
                      color: Color(0x3f000000),
                      offset: Offset(0 * fem, 4 * fem),
                      blurRadius: 2 * fem,
                    ),
                    BoxShadow(
                      color: Color(0x3f000000),
                      offset: Offset(0 * fem, 4 * fem),
                      blurRadius: 2 * fem,
                    ),
                  ],
                ),
              ),
              Container(
                // hawaiian4YW (25:117)
                margin:
                    EdgeInsets.fromLTRB(0 * fem, 0 * fem, 17 * fem, 36 * fem),
                child: Text(
                  'HAWAIIAN',
                  style: SafeGoogleFont(
                    'JejuMyeongjo',
                    fontSize: 20 * ffem,
                    fontWeight: FontWeight.w400,
                    height: 1.2575 * ffem / fem,
                    color: Color(0xff000000),
                  ),
                ),
              ),
              Container(
                // quantityMnW (25:119)
                margin:
                    EdgeInsets.fromLTRB(0 * fem, 0 * fem, 282 * fem, 19 * fem),
                child: Text(
                  'Quantity :',
                  style: SafeGoogleFont(
                    'JejuMyeongjo',
                    fontSize: 20 * ffem,
                    fontWeight: FontWeight.w400,
                    height: 1.2575 * ffem / fem,
                    color: Color(0xff000000),
                  ),
                ),
              ),
              Container(
                // autogroupsnq4sVx (CwHHyCz8U7TJAYXoezsNq4)
                margin: EdgeInsets.fromLTRB(
                    131 * fem, 0 * fem, 131 * fem, 23 * fem),
                width: double.infinity,
                height: 30 * fem,
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Container(
                      // autogrouphgqeBFk (CwHJ9crSwUMdL9h641hGQe)
                      margin: EdgeInsets.fromLTRB(
                          0 * fem, 0 * fem, 10 * fem, 0 * fem),
                      width: 46 * fem,
                      height: double.infinity,
                      child: Stack(
                        children: [
                          Positioned(
                            // rectangle11TUA (25:120)
                            left: 0 * fem,
                            top: 0 * fem,
                            child: Align(
                              child: SizedBox(
                                width: 46 * fem,
                                height: 27 * fem,
                                child: Container(
                                  decoration: BoxDecoration(
                                    border:
                                        Border.all(color: Color(0xff000000)),
                                    color: Color(0xffd9d9d9),
                                  ),
                                ),
                              ),
                            ),
                          ),
                          Positioned(
                            // uqx (25:121)
                            left: 14 * fem,
                            top: 4 * fem,
                            child: Align(
                              child: SizedBox(
                                width: 10 * fem,
                                height: 26 * fem,
                                child: Text(
                                  '+',
                                  style: SafeGoogleFont(
                                    'JejuMyeongjo',
                                    fontSize: 20 * ffem,
                                    fontWeight: FontWeight.w400,
                                    height: 1.2575 * ffem / fem,
                                    color: Color(0xff000000),
                                  ),
                                ),
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                    Container(
                      // autogroupwryswne (CwHJDcjnZVtPoy4kN8WRYS)
                      width: 46 * fem,
                      height: double.infinity,
                      child: Stack(
                        children: [
                          Positioned(
                            // rectangle12Hbc (25:122)
                            left: 0 * fem,
                            top: 0 * fem,
                            child: Align(
                              child: SizedBox(
                                width: 46 * fem,
                                height: 27 * fem,
                                child: Container(
                                  decoration: BoxDecoration(
                                    border:
                                        Border.all(color: Color(0xff000000)),
                                    color: Color(0xffd9d9d9),
                                  ),
                                ),
                              ),
                            ),
                          ),
                          Positioned(
                            // Bwt (25:123)
                            left: 16 * fem,
                            top: 4 * fem,
                            child: Align(
                              child: SizedBox(
                                width: 7 * fem,
                                height: 26 * fem,
                                child: Text(
                                  '-',
                                  style: SafeGoogleFont(
                                    'JejuMyeongjo',
                                    fontSize: 20 * ffem,
                                    fontWeight: FontWeight.w400,
                                    height: 1.2575 * ffem / fem,
                                    color: Color(0xff000000),
                                  ),
                                ),
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
              Container(
                  child: Padding(
                padding: EdgeInsets.only(right: 260),
                child: Text('Size:',
                    style: TextStyle(
                      color: Colors.black,
                      fontSize: 15,
                    )),
              )),
              Container(
                child: Center(
                  child: Padding(
                    padding: EdgeInsets.only(left: 50),
                    child: SizedBox(
                      height: 80,
                      width: 300,
                      child: Row(
                        children: [
                          TextButton(
                              onPressed: () {},
                              style: ButtonStyle(
                                  shape: MaterialStateProperty.all<
                                          RoundedRectangleBorder>(
                                      RoundedRectangleBorder(
                                          borderRadius: BorderRadius.zero,
                                          side: BorderSide(
                                              color: Colors.black)))),
                              child: Text('6',
                                  style: TextStyle(
                                    color: Colors.black,
                                  ))),
                          TextButton(
                              onPressed: () {},
                              style: ButtonStyle(
                                  shape: MaterialStateProperty.all<
                                          RoundedRectangleBorder>(
                                      RoundedRectangleBorder(
                                          borderRadius: BorderRadius.zero,
                                          side: BorderSide(
                                              color: Colors.black)))),
                              child: Text('8',
                                  style: TextStyle(
                                    color: Colors.black,
                                  ))),
                          TextButton(
                              onPressed: () {},
                              style: ButtonStyle(
                                  shape: MaterialStateProperty.all<
                                          RoundedRectangleBorder>(
                                      RoundedRectangleBorder(
                                          borderRadius: BorderRadius.zero,
                                          side: BorderSide(
                                              color: Colors.black)))),
                              child: Text('12',
                                  style: TextStyle(
                                    color: Colors.black,
                                  ))),
                        ],
                      ),
                    ),
                  ),
                ),
              ),
              Container(
                // autogroupalfqgdk (CwHJMXgGX6LFBBsF4paLfQ)
                margin:
                    EdgeInsets.fromLTRB(127 * fem, 0 * fem, 137 * fem, 0 * fem),
                padding: EdgeInsets.fromLTRB(
                    27 * fem, 32 * fem, 26.07 * fem, 33 * fem),
                width: double.infinity,
                decoration: BoxDecoration(
                  border: Border.all(color: Color(0xff000000)),
                  color: Color(0xffd9d9d9),
                  borderRadius: BorderRadius.circular(50 * fem),
                ),
                child: Center(
                  // vectoryMx (25:126)
                  child: SizedBox(
                    width: 46.93 * fem,
                    height: 35 * fem,
                    child: TextButton(
                      onPressed: () {
                        Navigator.push(context,
                            MaterialPageRoute(builder: (BuildContext context) {
                          return Purchased();
                        }));
                      },
                      style: TextButton.styleFrom(
                        padding: EdgeInsets.zero,
                      ),
                      child: Image.asset(
                        'assets/page-1/images/vector-2EA.png',
                        width: 46.93 * fem,
                        height: 35 * fem,
                      ),
                    ),
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
