import 'package:flutter/material.dart';
import 'package:flutter/gestures.dart';
import 'dart:ui';
import 'package:google_fonts/google_fonts.dart';
import 'package:myapp/page-1/home.dart';
import 'package:myapp/utils.dart';

class History extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    double baseWidth = 430;
    double fem = MediaQuery.of(context).size.width / baseWidth;
    double ffem = fem * 0.97;
    return Material(
        child: Container(
      width: double.infinity,
      child: Container(
        // historyQFY (25:143)
        padding: EdgeInsets.fromLTRB(63 * fem, 161 * fem, 63 * fem, 0 * fem),
        width: double.infinity,
        decoration: BoxDecoration(
          color: Color(0xffeddfdf),
        ),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Container(
              // historyWpN (25:144)
              margin:
                  EdgeInsets.fromLTRB(30 * fem, 0 * fem, 26 * fem, 40 * fem),
              child: Text(
                'HISTORY',
                style: SafeGoogleFont(
                  'JejuMyeongjo',
                  fontSize: 40 * ffem,
                  fontWeight: FontWeight.w400,
                  height: 1.2575 * ffem / fem,
                  color: Color(0xff000000),
                ),
              ),
            ),
            Container(
              // autogroupvmmyd8J (CwHKJqRSwnqX9KXt47VMMY)
              margin: EdgeInsets.fromLTRB(0 * fem, 0 * fem, 0 * fem, 32 * fem),
              padding:
                  EdgeInsets.fromLTRB(40 * fem, 9 * fem, 11 * fem, 5 * fem),
              width: double.infinity,
              decoration: BoxDecoration(
                border: Border.all(color: Color(0xff000000)),
                color: Color(0xffd9d9d9),
              ),
              child: Align(
                // pepperoniquantity0212231ifY (25:153)
                alignment: Alignment.centerLeft,
                child: SizedBox(
                  child: Container(
                      constraints: BoxConstraints(
                        maxWidth: 213 * fem,
                      ),
                      child: SizedBox(
                        width: 223 * fem,
                        height: 71 * fem,
                        child: Row(
                          children: [
                            Text(
                              'PEPPERONI\n02-12-23',
                              style: SafeGoogleFont(
                                'JejuMyeongjo',
                                fontSize: 14 * ffem,
                                fontWeight: FontWeight.w400,
                                height: 1.2575 * ffem / fem,
                                color: Color(0xff000000),
                              ),
                            ),
                            Spacer(),
                            Text(
                              'QUANTITY\n          1',
                              style: SafeGoogleFont(
                                'JejuMyeongjo',
                                fontSize: 14 * ffem,
                                fontWeight: FontWeight.w400,
                                height: 1.2575 * ffem / fem,
                                color: Color(0xff000000),
                              ),
                            ),
                          ],
                        ),
                      )),
                ),
              ),
            ),
            Container(
              // autogroupvmmyd8J (CwHKJqRSwnqX9KXt47VMMY)
              margin: EdgeInsets.fromLTRB(0 * fem, 0 * fem, 0 * fem, 32 * fem),
              padding:
                  EdgeInsets.fromLTRB(40 * fem, 9 * fem, 11 * fem, 5 * fem),
              width: double.infinity,
              decoration: BoxDecoration(
                border: Border.all(color: Color(0xff000000)),
                color: Color(0xffd9d9d9),
              ),
              child: Align(
                // pepperoniquantity0212231ifY (25:153)
                alignment: Alignment.centerLeft,
                child: SizedBox(
                  child: Container(
                      constraints: BoxConstraints(
                        maxWidth: 213 * fem,
                      ),
                      child: SizedBox(
                        width: 223 * fem,
                        height: 71 * fem,
                        child: Row(
                          children: [
                            Text(
                              '(PEPPERONI,\nBBQ CHICKEN\n05-22-23',
                              style: SafeGoogleFont(
                                'JejuMyeongjo',
                                fontSize: 14 * ffem,
                                fontWeight: FontWeight.w400,
                                height: 1.2575 * ffem / fem,
                                color: Color(0xff000000),
                              ),
                            ),
                            Spacer(),
                            Text(
                              'QUANTITY\n          2',
                              style: SafeGoogleFont(
                                'JejuMyeongjo',
                                fontSize: 14 * ffem,
                                fontWeight: FontWeight.w400,
                                height: 1.2575 * ffem / fem,
                                color: Color(0xff000000),
                              ),
                            ),
                          ],
                        ),
                      )),
                ),
              ),
            ),
            Container(
              // autogroupvmmyd8J (CwHKJqRSwnqX9KXt47VMMY)
              margin: EdgeInsets.fromLTRB(0 * fem, 0 * fem, 0 * fem, 32 * fem),
              padding:
                  EdgeInsets.fromLTRB(40 * fem, 9 * fem, 11 * fem, 5 * fem),
              width: double.infinity,
              decoration: BoxDecoration(
                border: Border.all(color: Color(0xff000000)),
                color: Color(0xffd9d9d9),
              ),
              child: Align(
                // pepperoniquantity0212231ifY (25:153)
                alignment: Alignment.centerLeft,
                child: SizedBox(
                  child: Container(
                      constraints: BoxConstraints(
                        maxWidth: 213 * fem,
                      ),
                      child: SizedBox(
                        width: 223 * fem,
                        height: 71 * fem,
                        child: Row(
                          children: [
                            Text(
                              '(HAWAIIAN)\n07-29-23',
                              style: SafeGoogleFont(
                                'JejuMyeongjo',
                                fontSize: 14 * ffem,
                                fontWeight: FontWeight.w400,
                                height: 1.2575 * ffem / fem,
                                color: Color(0xff000000),
                              ),
                            ),
                            Spacer(),
                            Text(
                              'QUANTITY\n          1',
                              style: SafeGoogleFont(
                                'JejuMyeongjo',
                                fontSize: 14 * ffem,
                                fontWeight: FontWeight.w400,
                                height: 1.2575 * ffem / fem,
                                color: Color(0xff000000),
                              ),
                            ),
                          ],
                        ),
                      )),
                ),
              ),
            ),
            Container(
              // autogroupvmmyd8J (CwHKJqRSwnqX9KXt47VMMY)
              margin: EdgeInsets.fromLTRB(0 * fem, 0 * fem, 0 * fem, 32 * fem),
              padding:
                  EdgeInsets.fromLTRB(40 * fem, 9 * fem, 11 * fem, 5 * fem),
              width: double.infinity,
              decoration: BoxDecoration(
                border: Border.all(color: Color(0xff000000)),
                color: Color(0xffd9d9d9),
              ),
              child: Align(
                // pepperoniquantity0212231ifY (25:153)
                alignment: Alignment.centerLeft,
                child: SizedBox(
                  child: Container(
                      constraints: BoxConstraints(
                        maxWidth: 213 * fem,
                      ),
                      child: SizedBox(
                        width: 223 * fem,
                        height: 71 * fem,
                        child: Row(
                          children: [
                            Text(
                              '(VEGIE)\n10-01-23',
                              style: SafeGoogleFont(
                                'JejuMyeongjo',
                                fontSize: 14 * ffem,
                                fontWeight: FontWeight.w400,
                                height: 1.2575 * ffem / fem,
                                color: Color(0xff000000),
                              ),
                            ),
                            Spacer(),
                            Text(
                              'QUANTITY\n          1',
                              style: SafeGoogleFont(
                                'JejuMyeongjo',
                                fontSize: 14 * ffem,
                                fontWeight: FontWeight.w400,
                                height: 1.2575 * ffem / fem,
                                color: Color(0xff000000),
                              ),
                            ),
                          ],
                        ),
                      )),
                ),
              ),
            ),
            Container(
                // autogroupswae77L (CwHKeaMZCYeJmAZhr4swae)
                margin:
                    EdgeInsets.fromLTRB(95 * fem, 0 * fem, 95 * fem, 0 * fem),
                padding:
                    EdgeInsets.fromLTRB(40 * fem, 7 * fem, 0 * fem, 2 * fem),
                width: double.infinity,
                decoration: BoxDecoration(
                  border: Border.all(color: Color(0xff000000)),
                  color: Color(0xffd9d9d9),
                ),
                child: GestureDetector(
                  onTap: () {
                    Navigator.push(context,
                        MaterialPageRoute(builder: (BuildContext context) {
                      return Home();
                    }));
                  },
                  child: Text(
                    'HOME',
                    style: SafeGoogleFont(
                      'JejuMyeongjo',
                      fontSize: 14 * ffem,
                      fontWeight: FontWeight.w400,
                      height: 1.2575 * ffem / fem,
                      color: Color(0xff000000),
                    ),
                  ),
                )),
          ],
        ),
      ),
    ));
  }
}
