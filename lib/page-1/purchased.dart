import 'package:flutter/material.dart';
import 'package:flutter/gestures.dart';
import 'dart:ui';
import 'package:google_fonts/google_fonts.dart';
import 'package:myapp/page-1/history.dart';
import 'package:myapp/page-1/home.dart';
import 'package:myapp/utils.dart';

class Purchased extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    double baseWidth = 430;
    double fem = MediaQuery.of(context).size.width / baseWidth;
    double ffem = fem * 0.97;
    return Material(
        child: Container(
      width: double.infinity,
      child: Container(
        // purchasedrGe (25:129)
        padding: EdgeInsets.fromLTRB(56 * fem, 206 * fem, 0 * fem, 0 * fem),
        width: double.infinity,
        decoration: BoxDecoration(
          color: Color(0xffeddfdf),
        ),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Container(
              // thankyouforpurchasingx4n (25:130)
              margin:
                  EdgeInsets.fromLTRB(0 * fem, 0 * fem, 57 * fem, 252 * fem),
              constraints: BoxConstraints(
                maxWidth: 281 * fem,
              ),
              child: Text(
                'THANK YOU \nFOR \nPURCHASING',
                textAlign: TextAlign.center,
                style: TextStyle(
                  fontSize: 40 * ffem,
                  fontWeight: FontWeight.w400,
                  height: 1.2575 * ffem / fem,
                  color: Color(0xff000000),
                ),
              ),
            ),
            Container(
              // autogroupg9741ok (CwHJgGe3NLkqg5Z9nFG974)
              width: double.infinity,
              height: 27 * fem,
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Container(
                      // autogroupthuaZ4a (CwHJmWzJFFWr467CwXTHUA)
                      margin: EdgeInsets.fromLTRB(
                          0 * fem, 0 * fem, 89 * fem, 0 * fem),
                      padding: EdgeInsets.fromLTRB(
                          36 * fem, 7 * fem, 42 * fem, 2 * fem),
                      height: double.infinity,
                      decoration: BoxDecoration(
                        border: Border.all(color: Color(0xff000000)),
                        color: Color(0xffd9d9d9),
                      ),
                      child: GestureDetector(
                        onTap: () {
                          Navigator.push(context, MaterialPageRoute(
                              builder: (BuildContext context) {
                            return Home();
                          }));
                        },
                        child: Text(
                          'HOME',
                          style: SafeGoogleFont(
                            'JejuMyeongjo',
                            fontSize: 14 * ffem,
                            fontWeight: FontWeight.w400,
                            height: 1.2575 * ffem / fem,
                            color: Color(0xff000000),
                          ),
                        ),
                      )),
                  Container(
                      // autogroupcqvgQaz (CwHJr1roa2ji6P9pnucqVg)
                      padding: EdgeInsets.fromLTRB(
                          15 * fem, 7 * fem, 28 * fem, 2 * fem),
                      height: double.infinity,
                      decoration: BoxDecoration(
                        border: Border.all(color: Color(0xff000000)),
                        color: Color(0xffd9d9d9),
                      ),
                      child: GestureDetector(
                        onTap: () {
                          Navigator.push(context, MaterialPageRoute(
                              builder: (BuildContext context) {
                            return History();
                          }));
                        },
                        child: Text(
                          'MY HISTORY',
                          style: SafeGoogleFont(
                            'JejuMyeongjo',
                            fontSize: 14 * ffem,
                            fontWeight: FontWeight.w400,
                            height: 1.2575 * ffem / fem,
                            color: Color(0xff000000),
                          ),
                        ),
                      )),
                ],
              ),
            ),
          ],
        ),
      ),
    ));
  }
}
