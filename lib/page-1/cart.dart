import 'package:flutter/material.dart';
import 'package:flutter/gestures.dart';
import 'dart:ui';
import 'package:google_fonts/google_fonts.dart';
import 'package:myapp/page-1/purchased.dart';
import 'package:myapp/utils.dart';

class Cart extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    double baseWidth = 430;
    double fem = MediaQuery.of(context).size.width / baseWidth;
    double ffem = fem * 0.97;
    return Material(
        child: Container(
      width: double.infinity,
      child: Container(
        // cartaUN (1:39)
        padding: EdgeInsets.fromLTRB(15 * fem, 85 * fem, 15 * fem, 0 * fem),
        width: double.infinity,
        decoration: BoxDecoration(
          color: Color(0xffeddfdf),
        ),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Container(
              // rectangle4Hdg (1:73)
              margin: EdgeInsets.fromLTRB(15 * fem, 0 * fem, 0 * fem, 0 * fem),
              child: TextButton(
                onPressed: () {},
                style: TextButton.styleFrom(
                  padding: EdgeInsets.zero,
                ),
                child: Container(
                  width: 135 * fem,
                  height: 147 * fem,
                  child: ClipRRect(
                    borderRadius: BorderRadius.circular(30 * fem),
                    child: Image.asset(
                      'assets/page-1/images/rectangle-4-xv2.png',
                      fit: BoxFit.cover,
                    ),
                  ),
                ),
              ),
            ),
            Container(
              // mycartZbC (1:74)
              margin:
                  EdgeInsets.fromLTRB(40 * fem, 15 * fem, 16 * fem, 26 * fem),
              child: Text(
                'MY CART ',
                style: SafeGoogleFont(
                  'JejuMyeongjo',
                  fontSize: 25 * ffem,
                  fontWeight: FontWeight.w400,
                  height: 1.2575 * ffem / fem,
                  color: Color(0xff000000),
                ),
              ),
            ),
            Container(
              // autogrouppvswgA2 (CwHGVFTMcYYorV16wUPVSW)
              margin: EdgeInsets.fromLTRB(0 * fem, 0 * fem, 62 * fem, 28 * fem),
              width: double.infinity,
              height: 125 * fem,
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.end,
                children: [
                  Container(
                    // autogroupacrinTx (CwHGdL4E94E2QXhFVFaCri)
                    margin: EdgeInsets.fromLTRB(
                        0 * fem, 0 * fem, 18 * fem, 20 * fem),
                    width: 43 * fem,
                    height: 41 * fem,
                    child: Image.asset(
                      'assets/page-1/images/auto-group-acri.png',
                      width: 43 * fem,
                      height: 41 * fem,
                    ),
                  ),
                  Container(
                    // autogrouphj5x6Dk (CwHGkk1YPtynDGpnegHj5x)
                    width: 277 * fem,
                    height: double.infinity,
                    child: Stack(
                      children: [
                        Positioned(
                          // rectangle7SHc (1:75)
                          left: 0 * fem,
                          top: 45 * fem,
                          child: Align(
                            child: SizedBox(
                              width: 277 * fem,
                              height: 80 * fem,
                              child: Container(
                                decoration: BoxDecoration(
                                  border: Border.all(color: Color(0xff000000)),
                                  color: Color(0xffd9d9d9),
                                ),
                              ),
                            ),
                          ),
                        ),
                        Positioned(
                          // detailsXZx (1:77)
                          left: 3 * fem,
                          top: 0 * fem,
                          child: Align(
                            child: SizedBox(
                              width: 80 * fem,
                              height: 50 * fem,
                              child: Text(
                                'details :',
                                style: TextStyle(
                                  fontSize: 20 * ffem,
                                  fontWeight: FontWeight.w400,
                                  height: 1.25 * ffem / fem,
                                  color: Color(0xff000000),
                                ),
                              ),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
            Container(
              // autogroupnneiBuQ (CwHH14w1CpoCGHQuSGNNei)
              margin: EdgeInsets.fromLTRB(0 * fem, 0 * fem, 62 * fem, 28 * fem),
              width: double.infinity,
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Container(
                    // autogroupb6qcJj8 (CwHH7u4xUTFAv8vf8tB6qc)
                    margin: EdgeInsets.fromLTRB(
                        0 * fem, 0 * fem, 18 * fem, 1 * fem),
                    width: 43 * fem,
                    height: 41 * fem,
                    child: Image.asset(
                      'assets/page-1/images/auto-group-b6qc.png',
                      width: 43 * fem,
                      height: 41 * fem,
                    ),
                  ),
                  Container(
                    // rectangle81tS (1:78)
                    width: 277 * fem,
                    height: 80 * fem,
                    decoration: BoxDecoration(
                      border: Border.all(color: Color(0xff000000)),
                      color: Color(0xffd9d9d9),
                    ),
                  ),
                ],
              ),
            ),
            Container(
              // autogrouppqyjkb8 (CwHHDttxuVYKdrz9c4PqYJ)
              margin: EdgeInsets.fromLTRB(0 * fem, 0 * fem, 62 * fem, 21 * fem),
              width: double.infinity,
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Container(
                    // autogrouphmgrsve (CwHHL9DZBugX8pt7qsHmgr)
                    margin: EdgeInsets.fromLTRB(
                        0 * fem, 0 * fem, 18 * fem, 1 * fem),
                    width: 43 * fem,
                    height: 41 * fem,
                    child: Image.asset(
                      'assets/page-1/images/auto-group-hmgr.png',
                      width: 43 * fem,
                      height: 41 * fem,
                    ),
                  ),
                  Container(
                    // rectangle9PPC (1:79)
                    width: 277 * fem,
                    height: 80 * fem,
                    decoration: BoxDecoration(
                      border: Border.all(color: Color(0xff000000)),
                      color: Color(0xffd9d9d9),
                    ),
                  ),
                ],
              ),
            ),
            Container(
              // autogroupguo4iwG (CwHHRoinV7WwUuAJcsGuo4)
              margin: EdgeInsets.fromLTRB(61 * fem, 0 * fem, 62 * fem, 0 * fem),
              width: double.infinity,
              height: 50 * fem,
              child: Stack(
                children: [
                  Positioned(
                    // rectangle10FRQ (1:80)
                    left: 0 * fem,
                    top: 7 * fem,
                    child: Align(
                      child: SizedBox(
                        width: 277 * fem,
                        height: 36 * fem,
                        child: TextButton(
                          onPressed: () {
                            Navigator.push(context, MaterialPageRoute(
                                builder: (BuildContext context) {
                              return Purchased();
                            }));
                          },
                          style: TextButton.styleFrom(
                            padding: EdgeInsets.zero,
                          ),
                          child: Image.asset(
                            'assets/page-1/images/rectangle-10.png',
                            width: 277 * fem,
                            height: 36 * fem,
                          ),
                        ),
                      ),
                    ),
                  ),
                  Positioned(
                    // placeorderjLa (1:81)
                    left: 95 * fem,
                    top: 12 * fem,
                    child: Align(
                      child: SizedBox(
                        width: 123 * fem,
                        height: 50 * fem,
                        child: Text(
                          'Place order',
                          style: TextStyle(
                            fontSize: 20 * ffem,
                            fontWeight: FontWeight.w400,
                            height: 1.25 * ffem / fem,
                            color: Color(0xff000000),
                          ),
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    ));
  }
}
