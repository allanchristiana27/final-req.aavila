import 'package:flutter/material.dart';
import 'package:flutter/gestures.dart';
import 'dart:ui';
import 'package:google_fonts/google_fonts.dart';
import 'package:myapp/page-1/home.dart';
import 'package:myapp/utils.dart';

class Register extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    double baseWidth = 430;
    double fem = MediaQuery.of(context).size.width / baseWidth;
    double ffem = fem * 0.97;
    return Material(
      child: Container(
        width: double.infinity,
        child: Container(
          // register6xE (1:35)
          padding: EdgeInsets.fromLTRB(66 * fem, 76 * fem, 66 * fem, 0 * fem),
          width: double.infinity,
          decoration: BoxDecoration(
            color: Color(0xffeddfdf),
          ),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Container(
                // frame2o5x (1:54)
                margin:
                    EdgeInsets.fromLTRB(81 * fem, 0 * fem, 82 * fem, 20 * fem),
                width: double.infinity,
                child: Center(
                  // rectangle5XGr (25:104)
                  child: SizedBox(
                    width: 135 * fem,
                    height: 147 * fem,
                    child: ClipRRect(
                      borderRadius: BorderRadius.circular(30 * fem),
                      child: Image.asset(
                        'assets/page-1/images/rectangle-5.png',
                        fit: BoxFit.cover,
                      ),
                    ),
                  ),
                ),
              ),
              Container(
                // usernameDQa (1:45)
                margin:
                    EdgeInsets.fromLTRB(0 * fem, 0 * fem, 225 * fem, 0 * fem),
                child: Text(
                  'Username',
                  style: TextStyle(
                    fontSize: 14 * ffem,
                    fontWeight: FontWeight.w400,
                    height: 1.2575 * ffem / fem,
                    color: Color(0xff000000),
                  ),
                ),
              ),
              Container(
                // rectangle4WuU (1:42)
                margin: EdgeInsets.fromLTRB(0 * fem, 0 * fem, 0 * fem, 0 * fem),
                width: double.infinity,
                height: 40 * fem,
                child: TextField(
                  decoration: InputDecoration(
                    border: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(12),
                        borderSide: BorderSide(
                          color: Colors.black,
                        )),
                    filled: true,
                    fillColor: Color.fromARGB(255, 212, 212, 212),
                  ),
                ),
              ),
              Container(
                child: Align(
                  alignment: Alignment.centerLeft,
                  child: SizedBox(
                    width: 70 * fem,
                    height: 18 * fem,
                    child: Text(
                      'Password',
                      style: TextStyle(
                        fontSize: 14 * ffem,
                        fontWeight: FontWeight.w400,
                        height: 1.2575 * ffem / fem,
                        color: Color(0xff000000),
                      ),
                    ),
                  ),
                ),
              ),
              Container(
                child: Align(
                  child: SizedBox(
                    width: 298 * fem,
                    height: 40 * fem,
                    child: TextField(
                      decoration: InputDecoration(
                        border: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(12),
                            borderSide: BorderSide(
                              color: Colors.black,
                            )),
                        filled: true,
                        fillColor: Color.fromARGB(255, 212, 212, 212),
                      ),
                    ),
                  ),
                ),
              ),
              Container(
                child: Align(
                  alignment: Alignment.centerLeft,
                  child: SizedBox(
                    width: 150 * fem,
                    height: 18 * fem,
                    child: Text(
                      'Confirm Password',
                      style: TextStyle(
                        fontSize: 14 * ffem,
                        fontWeight: FontWeight.w400,
                        height: 1.2575 * ffem / fem,
                        color: Color(0xff000000),
                      ),
                    ),
                  ),
                ),
              ),
              Container(
                child: Align(
                  child: SizedBox(
                    width: 298 * fem,
                    height: 40 * fem,
                    child: TextField(
                      decoration: InputDecoration(
                        border: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(12),
                            borderSide: BorderSide(
                              color: Colors.black,
                            )),
                        filled: true,
                        fillColor: Color.fromARGB(255, 212, 212, 212),
                      ),
                    ),
                  ),
                ),
              ),
              Container(
                child: Align(
                  alignment: Alignment.centerLeft,
                  child: SizedBox(
                    width: 150 * fem,
                    height: 18 * fem,
                    child: Text(
                      'Full Name',
                      style: TextStyle(
                        fontSize: 14 * ffem,
                        fontWeight: FontWeight.w400,
                        height: 1.2575 * ffem / fem,
                        color: Color(0xff000000),
                      ),
                    ),
                  ),
                ),
              ),
              Container(
                child: Align(
                  child: SizedBox(
                    width: 298 * fem,
                    height: 40 * fem,
                    child: TextField(
                      decoration: InputDecoration(
                        border: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(12),
                            borderSide: BorderSide(
                              color: Colors.black,
                            )),
                        filled: true,
                        fillColor: Color.fromARGB(255, 212, 212, 212),
                      ),
                    ),
                  ),
                ),
              ),
              Container(
                child: Align(
                  alignment: Alignment.centerLeft,
                  child: SizedBox(
                    width: 150 * fem,
                    height: 18 * fem,
                    child: Text(
                      'Contact Number',
                      style: TextStyle(
                        fontSize: 14 * ffem,
                        fontWeight: FontWeight.w400,
                        height: 1.2575 * ffem / fem,
                        color: Color(0xff000000),
                      ),
                    ),
                  ),
                ),
              ),
              Container(
                child: Align(
                  child: SizedBox(
                    width: 298 * fem,
                    height: 40 * fem,
                    child: TextField(
                      decoration: InputDecoration(
                        border: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(12),
                            borderSide: BorderSide(
                              color: Colors.black,
                            )),
                        filled: true,
                        fillColor: Color.fromARGB(255, 212, 212, 212),
                      ),
                    ),
                  ),
                ),
              ),
              Container(
                child: Align(
                  alignment: Alignment.centerLeft,
                  child: SizedBox(
                    width: 150 * fem,
                    height: 18 * fem,
                    child: Text(
                      'Address',
                      style: TextStyle(
                        fontSize: 14 * ffem,
                        fontWeight: FontWeight.w400,
                        height: 1.2575 * ffem / fem,
                        color: Color(0xff000000),
                      ),
                    ),
                  ),
                ),
              ),
              Container(
                child: Align(
                  child: SizedBox(
                    width: 298 * fem,
                    height: 40 * fem,
                    child: TextField(
                      decoration: InputDecoration(
                        border: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(12),
                            borderSide: BorderSide(
                              color: Colors.black,
                            )),
                        filled: true,
                        fillColor: Color.fromARGB(255, 212, 212, 212),
                      ),
                    ),
                  ),
                ),
              ),
              SizedBox(
                height: 10,
              ),
              Container(
                padding:
                    EdgeInsets.fromLTRB(10 * fem, 10 * fem, 10 * fem, 10 * fem),
                width: 50,
                height: 50,
                decoration: BoxDecoration(
                  border: Border.all(color: Color(0xff000000)),
                  color: Color(0xffd9d9d9),
                  borderRadius: BorderRadius.circular(50 * fem),
                ),
                child: Center(
                  // vectoreZk (1:51)
                  child: SizedBox(
                    width: 46.93 * fem,
                    height: 35 * fem,
                    child: TextButton(
                      onPressed: () {
                        Navigator.push(context,
                            MaterialPageRoute(builder: (BuildContext context) {
                          return Home();
                        }));
                      },
                      style: TextButton.styleFrom(
                        padding: EdgeInsets.zero,
                      ),
                      child: Image.asset(
                        'assets/page-1/images/vector-xuC.png',
                        width: 46.93 * fem,
                        height: 35 * fem,
                      ),
                    ),
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
