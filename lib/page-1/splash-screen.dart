import 'package:flutter/material.dart';
import 'package:myapp/page-1/home.dart';
import 'package:myapp/page-1/register.dart';
import 'package:myapp/utils.dart';

class Splash extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    double baseWidth = 430;
    double fem = MediaQuery.of(context).size.width / baseWidth;
    double ffem = fem * 0.97;
    return Material(
      child: Container(
        width: double.infinity,
        child: Container(
          // splashscreenCoY (1:3)
          padding: EdgeInsets.fromLTRB(72 * fem, 48 * fem, 36 * fem, 20 * fem),
          width: double.infinity,
          decoration: BoxDecoration(
            color: Color(0xff000000),
            image: DecorationImage(
              fit: BoxFit.cover,
              image: AssetImage(
                'assets/page-1/images/b5c807e4ef3632fe835adf0cecb-1-bg.png',
              ),
            ),
          ),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Container(
                // autogroupkoyjjqQ (CwHLr3MosFjdbUvomrkoYJ)
                margin: EdgeInsets.fromLTRB(
                    76 * fem, 0 * fem, 111 * fem, 415 * fem),
                width: double.infinity,
                child: Center(
                  // rectangle43r6 (25:103)
                  child: SizedBox(
                    width: 135 * fem,
                    height: 125 * fem,
                    child: ClipRRect(
                      borderRadius: BorderRadius.circular(30 * fem),
                      child: Image.asset(
                        'assets/page-1/images/rectangle-4.png',
                        fit: BoxFit.cover,
                      ),
                    ),
                  ),
                ),
              ),
              Container(
                // autogrouprjwkAQv (CwHLwsXRjNoR8P6ePwrjwk)
                margin:
                    EdgeInsets.fromLTRB(0 * fem, 0 * fem, 35 * fem, 6 * fem),
                width: 287 * fem,
                height: 44 * fem,
                child: Stack(
                  children: [
                    Positioned(
                      // rectangle1UAi (1:15)
                      left: 0 * fem,
                      top: 14 * fem,
                      child: Align(
                        child: SizedBox(
                            width: 287 * fem,
                            height: 30 * fem,
                            child: TextField(
                              decoration: InputDecoration(
                                border: OutlineInputBorder(
                                    borderRadius: BorderRadius.circular(20),
                                    borderSide: BorderSide(
                                      color: Colors.black,
                                    )),
                                filled: true,
                                fillColor: Color.fromARGB(255, 212, 212, 212),
                              ),
                            )),
                      ),
                    ),
                    Positioned(
                      // emailorusernamemQi (1:34)
                      left: 9 * fem,
                      top: 0 * fem,
                      child: Align(
                        child: SizedBox(
                          width: 300 * fem,
                          height: 18 * fem,
                          child: Text(
                            'Email or Username',
                            style: SafeGoogleFont(
                              'JejuMyeongjo',
                              fontSize: 14 * ffem,
                              fontWeight: FontWeight.w400,
                              height: 1.2575 * ffem / fem,
                              color: Color(0xff000000),
                            ),
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
              Container(
                // autogroup74z2CF8 (CwHM4cqBiYdiBpfaB274z2)
                margin:
                    EdgeInsets.fromLTRB(0 * fem, 0 * fem, 35 * fem, 12 * fem),
                width: 287 * fem,
                height: 44 * fem,
                child: Stack(
                  children: [
                    Positioned(
                      // rectangle25Zp (1:16)
                      left: 0 * fem,
                      top: 14 * fem,
                      child: Align(
                        child: SizedBox(
                            width: 287 * fem,
                            height: 30 * fem,
                            child: TextField(
                              decoration: InputDecoration(
                                border: OutlineInputBorder(
                                    borderRadius: BorderRadius.circular(20),
                                    borderSide: BorderSide(
                                      color: Colors.black,
                                    )),
                                filled: true,
                                fillColor: Color.fromARGB(255, 212, 212, 212),
                              ),
                            )),
                      ),
                    ),
                    Positioned(
                      // passwordyfC (1:33)
                      left: 9 * fem,
                      top: 0 * fem,
                      child: Align(
                        child: SizedBox(
                          width: 300 * fem,
                          height: 18 * fem,
                          child: Text(
                            'Password',
                            style: SafeGoogleFont(
                              'JejuMyeongjo',
                              fontSize: 14 * ffem,
                              fontWeight: FontWeight.w400,
                              height: 1.2575 * ffem / fem,
                              color: Color(0xff000000),
                            ),
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
              Container(
                // frame3T4a (1:55)
                margin:
                    EdgeInsets.fromLTRB(92 * fem, 0 * fem, 127 * fem, 14 * fem),
                width: double.infinity,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Container(
                      // autogrouprv94ZtJ (CwHMdmYcfBVQL8tRbaRv94)
                      margin: EdgeInsets.fromLTRB(
                          0 * fem, 0 * fem, 0 * fem, 30 * fem),
                      padding: EdgeInsets.fromLTRB(
                          31 * fem, 7 * fem, 32 * fem, 6 * fem),
                      width: double.infinity,
                      decoration: BoxDecoration(
                        image: DecorationImage(
                          fit: BoxFit.cover,
                          image: AssetImage(
                            'assets/page-1/images/ellipse-2.png',
                          ),
                        ),
                      ),
                      child: Center(
                        // vector44N (27:3)
                        child: SizedBox(
                          width: 40 * fem,
                          height: 32 * fem,
                          child: TextButton(
                            onPressed: () {
                              Navigator.push(context, MaterialPageRoute(
                                  builder: (BuildContext context) {
                                return Home();
                              }));
                            },
                            style: TextButton.styleFrom(
                              padding: EdgeInsets.zero,
                            ),
                            child: Image.asset(
                              'assets/page-1/images/vector-4UN.png',
                              width: 40 * fem,
                              height: 32 * fem,
                            ),
                          ),
                        ),
                      ),
                    ),
                    Container(
                        // signupZWv (1:85)
                        margin: EdgeInsets.fromLTRB(
                            0 * fem, 0 * fem, 2 * fem, 0 * fem),
                        child: GestureDetector(
                          onTap: () {
                            Navigator.push(context, MaterialPageRoute(
                                builder: (BuildContext context) {
                              return Register();
                            }));
                          },
                          child: Text(
                            'Sign Up',
                            style: SafeGoogleFont(
                              'JejuMyeongjo',
                              fontSize: 18 * ffem,
                              fontWeight: FontWeight.w400,
                              height: 1.2575 * ffem / fem,
                              color: Color(0xff000000),
                            ),
                          ),
                        )),
                  ],
                ),
              ),
              Container(
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    TextButton(
                      onPressed: () {},
                      style: TextButton.styleFrom(
                        padding: EdgeInsets.zero,
                      ),
                      child: Container(
                        child: Image.asset(
                          'assets/page-1/images/vector-YTQ.png',
                          width: 59 * fem,
                          height: 56 * fem,
                        ),
                      ),
                    ),
                    SizedBox(
                      width: 30 * fem,
                    ),
                    TextButton(
                      // vectorfiN (1:30)
                      onPressed: () {},
                      style: TextButton.styleFrom(
                        padding: EdgeInsets.zero,
                      ),
                      child: Container(
                        width: 59 * fem,
                        height: 56 * fem,
                        child: Image.asset(
                          'assets/page-1/images/vector-x22.png',
                          width: 59 * fem,
                          height: 56 * fem,
                        ),
                      ),
                    ),
                    SizedBox(
                      width: 30 * fem,
                    ),
                    TextButton(
                      // vectoraKY (1:32)
                      onPressed: () {},
                      style: TextButton.styleFrom(
                        padding: EdgeInsets.zero,
                      ),
                      child: Container(
                        width: 59 * fem,
                        height: 56 * fem,
                        child: Image.asset(
                          'assets/page-1/images/vector-f2v.png',
                          width: 59 * fem,
                          height: 56 * fem,
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
